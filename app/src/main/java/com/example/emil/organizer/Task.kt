package com.example.emil.organizer

import android.os.Parcel
import android.os.Parcelable

/**
 * Created by Emil on 03.12.2016.
 */
data class Task(public val id : Long, public var task : String, public var desc : String, public var time: String) : Parcelable {

    constructor(instream: Parcel) : this(instream.readLong(), instream.readString(),instream.readString(), instream.readString()){ }

    override fun describeContents(): Int {
        return 0
    }

    override fun writeToParcel(dest: Parcel?, flags: Int) {
        dest?.writeLong(id)
        dest?.writeString(task)
        dest?.writeString(desc)
        dest?.writeString(time)
    }

    companion object {
        @JvmField final val CREATOR: Parcelable.Creator<Task> = object : Parcelable.Creator<Task> {
            override fun createFromParcel(source: Parcel): Task{
                return Task(source)
            }

            override fun newArray(size: Int): Array<Task?> {
                return arrayOfNulls(size)
            }
        }
    }
}