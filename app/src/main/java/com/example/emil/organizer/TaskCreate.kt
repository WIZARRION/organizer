package com.example.emil.organizer

import android.app.AlarmManager
import android.app.DialogFragment
import android.app.NotificationManager
import android.os.Bundle
import android.text.InputType
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import android.widget.Toast
import android.app.Notification;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import com.example.emil.organizer.R.mipmap.ic_launcher
import android.os.SystemClock.elapsedRealtime
import android.os.SystemClock
import android.content.Context.ALARM_SERVICE
import android.app.Activity;
import android.os.SystemClock.currentThreadTimeMillis
import android.view.Menu;
import android.view.MenuItem;









//import kotlinx.android.synthetic.main.new_task.*

/**
 * Created by Emil on 03.12.2016.
 */
class TaskCreate() : DialogFragment() {

    var position : Int = -1
    var adapter : Adapter? = null
    lateinit var taskView : TextView
    lateinit var descView : TextView
    lateinit var timeView : TextView
    lateinit var timeSet : TextView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if(savedInstanceState != null && savedInstanceState.containsKey("Position")) {
            position = savedInstanceState.getInt("Position")
        }
    }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View {
        dialog.setTitle(R.string.dialog_title)

        if(position > -1) {
            adapter = (activity as MainActivity).adapter
        }

        val view = inflater!!.inflate(R.layout.new_task, null)

        view.findViewById(R.id.saveBtn).setOnClickListener { onSave() }
        view.findViewById(R.id.cancelBtn).setOnClickListener { onCancel() }
        view.findViewById(R.id.noteTask).setOnClickListener { notificate() }

        taskView = view.findViewById(R.id.editTask) as TextView
        taskView.isCursorVisible = true
        taskView.isFocusableInTouchMode = true
        taskView.inputType = InputType.TYPE_CLASS_TEXT
        taskView.requestFocus()

        descView = view.findViewById(R.id.editDesc) as TextView
        descView.isCursorVisible = true
        descView.isFocusableInTouchMode = true
        descView.inputType = InputType.TYPE_CLASS_TEXT
        descView.requestFocus()

        timeView = view.findViewById(R.id.editTime) as TextView
        timeView.isCursorVisible = true
        timeView.isFocusableInTouchMode = true
        timeView.inputType = InputType.TYPE_CLASS_TEXT
        timeView.requestFocus()

        timeSet = view.findViewById(R.id.setTime) as TextView
        timeSet.isCursorVisible = true
        timeSet.isFocusableInTouchMode = true
        timeSet.inputType = InputType.TYPE_CLASS_TEXT
        timeSet.requestFocus()

        val record = adapter?.getItem(position)

        if(record != null){
            taskView.setText(record!!.task)
            descView.setText(record!!.desc)
            timeView.setText(record!!.time)
            timeSet.setText(record!!.time.substring(0,5))
        }
        return view!!
    }


    fun onSave(){
        val record = adapter?.getItem(position)
        if(record != null){
            record!!.task = taskView.text.toString()
            record!!.desc = descView.text.toString()
            record!!.time = timeView.text.toString()

            adapter?.update(record)
        }
        super.onDismiss(dialog)
    }

    fun onCancel(){
        super.onDismiss(dialog)
    }
    fun notificate()
    {
        var record = adapter?.getItem(position)
        var toast=Toast.makeText(adapter!!.context,"Уведомление для "+record!!.task+" создано!",Toast.LENGTH_SHORT)
        toast.show()
        var nm = adapter!!.context.getSystemService(Context.ALARM_SERVICE) as AlarmManager
        var intent = Intent(adapter!!.context, TimeNotification::class.java)
        var time=(Integer.parseInt(record!!.time.substring(0,2))*60+Integer.parseInt(record!!.time.substring(3,5))).toLong()*60*1000-System.currentTimeMillis()
        intent.putExtra("task",record!!.task)
        intent.putExtra("desc",record!!.desc)
        var pendingIntent = PendingIntent.getBroadcast(adapter!!.context, 0,
                intent, PendingIntent.FLAG_CANCEL_CURRENT)
        nm.cancel(pendingIntent)
        nm.set(AlarmManager.RTC_WAKEUP, System.currentTimeMillis()+time, pendingIntent);

    }
    override fun onSaveInstanceState(outState: Bundle?) {
        outState?.putInt("Position", position)
        super.onSaveInstanceState(outState)
    }

}