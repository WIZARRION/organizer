package com.example.emil.organizer

import android.content.Context
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper

/**
 * Created by Emil on 03.12.2016.
 */
class DataBase(context : Context) : SQLiteOpenHelper(context, "TaskBase", null, 1){
    override fun onCreate(db: SQLiteDatabase?) {
        if(db != null){
            db.execSQL("create table Tasks ("
                    + "taskID integer primary key autoincrement,"
                    + "taskName text,"
                    + "taskDesc text,"
                    + "time text " + ");")
        }
    }

    override fun onUpgrade(db: SQLiteDatabase?, oldVersion: Int, newVersion: Int) {

    }
}