package com.example.emil.organizer

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.view.ActionMode
import android.support.v7.widget.DefaultItemAnimator
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.SparseArray
import android.view.Menu
import android.view.MenuItem
import java.util.*
import android.app.AlarmManager
import android.content.Context.ALARM_SERVICE



class MainActivity : AppCompatActivity(), Adapter.MyViewHolder.ClickListener  {

    inner class ActionModeCallBack() : ActionMode.Callback{
        override fun onPrepareActionMode(mode: ActionMode?, menu: Menu?): Boolean {
            return false
        }

        override fun onActionItemClicked(mode: ActionMode?, item: MenuItem?): Boolean {
            if(item != null){
                when(item.itemId){
                    R.id.delete_slc -> {
                        adapter.removeItems()
                        mode?.finish()
                        return true
                    }
                    R.id.select_all ->{
                        adapter.select_all()
                        val count = adapter.selectedItemCount
                        if(count == 0){
                            mode?.finish()
                        }
                        else{
                            mode?.setTitle(count.toString())
                            mode?.invalidate()
                        }

                        return true
                    }
                    else -> {
                        return false
                    }
                }
            }
            return true
        }

        override fun onCreateActionMode(mode: ActionMode?, menu: Menu?): Boolean {
            if(mode != null && menu != null){
                mode.menuInflater.inflate(R.menu.select_menu,menu)
                return true
            }
            return true
        }

        override fun onDestroyActionMode(mode: ActionMode?) {
            adapter.clearSelection()
            actionMode = null
        }
    }
    lateinit private var data : ArrayList<Task>
    lateinit var recyclerView : RecyclerView
    var actionMode : ActionMode? = null
    lateinit var actionModeCallback : ActionModeCallBack
    lateinit var adapter : Adapter
    var taskRedact : TaskCreate? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        if(savedInstanceState == null || !savedInstanceState.containsKey("Adapter")) {
            adapter = Adapter(this, this)
        }
        else{
            adapter = savedInstanceState.getParcelable("Adapter")
            adapter.clickListener = this
            adapter.context = this
        }

        val layoutManager = LinearLayoutManager(this)
        layoutManager.orientation = LinearLayoutManager.VERTICAL
        layoutManager.scrollToPosition(0)



        recyclerView = findViewById(R.id.recycler_view) as RecyclerView
        recyclerView.layoutManager = layoutManager
        actionModeCallback = ActionModeCallBack()
        recyclerView.adapter = adapter
        recyclerView.itemAnimator = DefaultItemAnimator()

        if(adapter.selectedItemCount > 0){
            actionMode = startSupportActionMode(actionModeCallback)
            actionMode?.title = adapter.selectedItemCount.toString()
        }
    }

    override fun onSaveInstanceState(outState: Bundle?) {
        outState?.putParcelable("Adapter", adapter)
        super.onSaveInstanceState(outState)
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.main, menu)
        return true
    }

    fun toogleSelection(position : Int){
        adapter.toggleSelection(position)

        val count = adapter.selectedItemCount
        if(count == 0){
            actionMode?.finish()
            actionMode = null
        }
        else{
            actionMode?.setTitle(count.toString())
            actionMode?.invalidate()
        }
    }

    override fun onItemClicked(position: Int) {
        if(actionMode == null) {
            taskRedact = TaskCreate()
            if(taskRedact != null){
                taskRedact!!.position = position
                taskRedact!!.adapter = adapter
                taskRedact!!.show(fragmentManager,"TaskDialog")
            }
        }
        else{
            toogleSelection(position)
        }
    }

    override fun onItemLongClick(position: Int): Boolean {
        if(actionMode == null){
            actionMode = startSupportActionMode(actionModeCallback)
        }
        toogleSelection(position)
        return true
    }

    fun add_item(menuItem: MenuItem){
        var newTask=Task(-1,"Имя задачи","Описание","12-30 15.12.2016")
        taskRedact = TaskCreate()
        if(taskRedact != null){
            taskRedact!!.position = adapter.add(newTask)
            taskRedact!!.adapter = adapter
            taskRedact!!.show(fragmentManager,"AddDialog")
        }
    }




}
