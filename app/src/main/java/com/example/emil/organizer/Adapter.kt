package com.example.emil.organizer

import android.content.ContentValues
import android.content.Context
import android.database.Cursor
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteException
import android.os.Parcel
import android.os.Parcelable
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.util.SparseBooleanArray
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import java.util.*

/**
 * Created by Emil on 03.12.2016.
 */
class Adapter() : RecyclerView.Adapter<Adapter.MyViewHolder>(), Parcelable {

    override fun describeContents(): Int {
        return 0
    }

    override fun writeToParcel(dest: Parcel?, flags: Int) {
        dest?.writeList(data)
        dest?.writeSparseBooleanArray(selectedItems)
    }

    companion object {
        @JvmField final val CREATOR: Parcelable.Creator<Adapter> = object : Parcelable.Creator<Adapter> {
            override fun createFromParcel(source: Parcel): Adapter{
                return Adapter(source)
            }

            override fun newArray(size: Int): Array<Adapter?> {
                return arrayOfNulls(size)
            }
        }
    }



    class MyViewHolder : RecyclerView.ViewHolder, View.OnClickListener, View.OnLongClickListener{

        interface ClickListener{
            fun onItemClicked(position : Int)
            fun onItemLongClick(position : Int) : Boolean
        }

        var task: TextView? = null
        var desc: TextView? = null
        var time: TextView? = null
        var listener: ClickListener? = null

        constructor(holderView : View, clickListener : ClickListener) : super(holderView){
            task = holderView.findViewById(R.id.item_task) as TextView
            desc = holderView.findViewById(R.id.item_desc) as TextView
            time = holderView.findViewById(R.id.item_time) as TextView
            listener = clickListener

            holderView.isSelected = false

            holderView.setOnClickListener(this)
            holderView.setOnLongClickListener(this)
        }

        override fun onClick(v: View?) {
            listener?.onItemClicked(layoutPosition)
        }

        override fun onLongClick(v: View?): Boolean {
            if(listener != null){
                return listener!!.onItemLongClick(layoutPosition)
            }
            return false
        }
    }

    var data: ArrayList<Task> = ArrayList<Task>()
    private var selectedItems = SparseBooleanArray()
    lateinit var nb : DataBase
    lateinit var db : SQLiteDatabase
    lateinit var c : Cursor
    lateinit var clickListener: MyViewHolder.ClickListener
    lateinit var context : Context

    constructor(clickListener: MyViewHolder.ClickListener,
                 context : Context) : this(){
        this.clickListener = clickListener
        this.context = context
        try {
            nb = DataBase(context)
            db = nb.readableDatabase
            c = db.query("Tasks", null, null, null, null, null, null)
            Log.w("Load","start")
            if (c.moveToFirst()) {
                do {
                    var record = Task(
                            c.getLong(c.getColumnIndex("taskID")),
                            c.getString(c.getColumnIndex("taskName")),
                            c.getString(c.getColumnIndex("taskDesc")),
                            c.getString(c.getColumnIndex("time")))
                    data.add(record)
                    Log.w("Load","success")
                } while (c.moveToNext())
            }
        }
        catch (ex : SQLiteException){}
    }

    constructor(clickListener: MyViewHolder.ClickListener, context : Context, selectedItems : SparseBooleanArray,
                data : ArrayList<Task>) : this(){
        this.clickListener = clickListener
        this.selectedItems = selectedItems
        this.data = data
        this.context = context
    }
    constructor(instream: Parcel):this(){
        data = ArrayList<Task>()
        instream.readList(data,null)
        selectedItems = instream.readSparseBooleanArray()
    }

    val selectedItemCount : Int
        get() = selectedItems.size()

    override fun getItemCount(): Int {
        return data.size
    }

    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): MyViewHolder {
        val itemView = LayoutInflater.from(parent!!.context).inflate(R.layout.item_layout, parent, false)
        return MyViewHolder(itemView, clickListener)
    }

    override fun onBindViewHolder(holder: MyViewHolder?, position: Int) {
        if(holder != null){
            val record = data.get(position)
            holder.task?.setText(record.task)
            holder.desc?.setText(record.desc)
            holder.time?.setText(record.time)

            holder.itemView.isSelected = selectedItems.get(position, false)

        }
    }

    fun toggleSelection(position : Int) {
        if (selectedItems.get(position, false)) {
            selectedItems.delete(position)
        } else {
            selectedItems.put(position, true)
        }
        notifyItemChanged(position)
    }

    val selectedItemsList : ArrayList<Int>
        get() {val items = ArrayList<Int>(selectedItems.size());
            for (i in 0..selectedItems.size()-1) {
                items.add(selectedItems.keyAt(i))
            }
            return items}

    fun clearSelection() {
        var selection = selectedItemsList
        selectedItems.clear()
        for ( i in selection) {
            notifyItemChanged(i)
        }
    }

    fun getItem(position: Int) : Task{
        return data.get(position)
    }

    fun removeItem(position : Int){
        Log.w("Delete","start")
        try{
            db = nb.writableDatabase
            var cv = ContentValues()
            Log.w("Delete","success")
            db.delete("Tasks", "taskID=?", arrayOf(data.get(position).id.toString()))
        }
        catch (ex : SQLiteException){ }
        data.removeAt(position)
        notifyItemRemoved(position)
    }

    fun removeItems(){
        var selection = selectedItemsList
        selectedItems.clear()
        for ( i in selection.size-1 downTo 0) {
            removeItem(selection.get(i))
        }
        notifyDataSetChanged()
    }

    fun select_all(){
        if(selectedItemsList.size == data.size){
            clearSelection()
        }
        else{
            for(i in 0..data.size-1){
                if (selectedItems.get(i, false) == false) {
                    selectedItems.put(i, true)
                }
            }
        }
        notifyDataSetChanged()
    }

    fun add(newRecord : Task) : Int{
        try {
            db = nb.writableDatabase
            var cv = ContentValues()
            cv.put("taskName", newRecord.task)
            cv.put("taskDesc", newRecord.desc)
            cv.put("time", newRecord.time)
            var Id = db.insert("Tasks", null, cv)
            Log.w("Insert", data.indexOf(newRecord).toString())
            var toAdd = Task(Id,newRecord.task,newRecord.desc,newRecord.time)
            data.add(toAdd)
            notifyDataSetChanged()
            return data.indexOf(toAdd)
        }
        catch (ex : SQLiteException){
            Log.w("Insert", "failed")
            return -1
        }

    }

    fun update(record : Task){
        try {
            db = nb.writableDatabase
            var cv = ContentValues()
            cv.put("taskName", record.task)
            cv.put("taskDesc", record.desc)
            cv.put("time", record.time)
            db.update("Tasks",cv, "taskID=?", arrayOf(record.id.toString()))
            Log.w("Update", "success")
            notifyItemChanged(data.indexOf(record))
        }
        catch (ex : SQLiteException){ Log.w("Update", ex.toString())}
    }
}