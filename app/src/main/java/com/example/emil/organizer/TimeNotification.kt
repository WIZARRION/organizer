package com.example.emil.organizer

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.app.NotificationManager
import android.content.Context.NOTIFICATION_SERVICE
import android.app.PendingIntent
import android.app.AlarmManager
import android.app.Notification
import android.content.Context.ALARM_SERVICE





/**
 * Created by Emil on 07.12.2016.
 */
class TimeNotification : BroadcastReceiver() {
    override fun onReceive(context: Context, intent: Intent) {
        var nm = context.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        val builder = Notification.Builder(context)
        var task=intent.getStringExtra("task")
        var desc=intent.getStringExtra("desc")
        builder.setContentTitle(task)
        builder.setContentText(desc)
        builder.setSmallIcon(R.drawable.notification_template_icon_bg)
        var notification=builder.build()
        nm.notify(1, notification)
        val am = context.getSystemService(Context.ALARM_SERVICE) as AlarmManager
        val pendingIntent = PendingIntent.getBroadcast(context, 0,
                intent, PendingIntent.FLAG_CANCEL_CURRENT)
        am.set(AlarmManager.RTC_WAKEUP, System.currentTimeMillis(), pendingIntent)
    }
}